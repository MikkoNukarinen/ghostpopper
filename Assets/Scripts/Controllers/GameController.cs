﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    // Controls UI elements, and audio, related to game state, and player's buffs
    // Keeps score

    public Text scoreText;
    public Text gameOverText;
    public Text restartText;
    public Text quitText;
    public Button restartTextButton;
    public Button quitTextButton;
    public int score;

    AudioSource gameOverAudio;

    void Awake () {

        restartTextButton = restartText.GetComponent<Button> ();    
        quitTextButton = quitText.GetComponent<Button> ();
        gameOverAudio = GetComponent<AudioSource> ();
    }

    void Start () {

        gameOverText.text = "";
        restartText.text = "";
        quitText.text = "";
        score = 0;    
    }



    public void increaseScore () {

        score++;
        scoreText.text = "SCORE: " + score;
    }

    public void showGameOver () {
        
        // Make sure players can't accidentally restart or quit the game

        restartTextButton.interactable = true;
        quitTextButton.interactable = true;

        gameOverText.text = "GAME OVER";
        restartText.text = "RESTART";
        quitText.text = "QUIT GAME";

        gameOverAudio.Play ();
    }

    // Reload or quit the game depending on player's bravery

    public void reloadLevel () {

        SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
    }

    public void quitGame () {

        Application.Quit ();
    }
}
