﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public Transform playerTransform;
    public float smoothing = 5f;

    GameObject player;
    PlayerHealth playerHealth;
    Vector3 offset;

    void Awake () {
        player = GameObject.FindGameObjectWithTag ("Player");
        playerTransform = player.GetComponent<Transform> ();
        playerHealth = player.GetComponent<PlayerHealth> (); 
    }
    void Start () {
        offset = transform.position - playerTransform.position;    
    }

    void FixedUpdate () {
        
        if (playerHealth.currentHealth > 0) {
            Vector3 targetPosition = playerTransform.position + offset;
            transform.position = Vector3.Lerp (transform.position, targetPosition, smoothing * Time.deltaTime);
        }        
    }
}
