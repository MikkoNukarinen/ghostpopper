﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour {

    // Both the enemies and pickups are spawned here
    // Pickups are spawned at a fixed rate
    // Spawn rate of enemies increases as the game progresses

    public PlayerHealth playerHealth;
    public GameObject enemy;  
    public Transform [] spawnPoints;
    public GameObject [] pickupSpawnPoints;
    public GameObject [] buffs;

    float timer;
    float spawnTime = 3f;
    float pickupTrigger = 10f;
    float newPhaseTrigger = 18f;     

    void Awake () {

        playerHealth = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerHealth> ();
    }

    void Start () {
        
        InvokeRepeating ("Spawn", spawnTime, spawnTime);
    }

    void Update () {

        timer += Time.deltaTime;        

        if (timer >= pickupTrigger) {
            pickupTrigger += 10f;
            SpawnBuffs ();
        }
        
        if (timer >= newPhaseTrigger) {
            newPhaseTrigger += 18f;
            CancelInvoke ();
            spawnTime *= 0.9f;
            InvokeRepeating ("Spawn", spawnTime, spawnTime);            
        }

        Debug.Log (spawnTime);
    }

    // Spawn enemies at random spawn point
    
    void Spawn () {

        if (playerHealth.currentHealth <= 0) {
            return;
        }

        int spawnPointIndex = Random.Range (0, spawnPoints.Length);

        Instantiate (enemy, spawnPoints [spawnPointIndex].position, spawnPoints [spawnPointIndex].rotation);
    }
    
    // Spawn random pick up at random spawnpoint, if it doesn't already contain a pick up

    void SpawnBuffs () {
        
        int buffIndex = Random.Range (0, buffs.Length);
        int pickupSpawnPointIndex = Random.Range (0, pickupSpawnPoints.Length);

        if (pickupSpawnPoints [pickupSpawnPointIndex].GetComponent<PickUpSpawnLimiter> ().spawnAvailable) {

            Instantiate (buffs [buffIndex], pickupSpawnPoints [pickupSpawnPointIndex].transform.position, pickupSpawnPoints [pickupSpawnPointIndex].transform.rotation);
            pickupSpawnPoints [pickupSpawnPointIndex].GetComponent<PickUpSpawnLimiter> ().setAvailabilityToFalse ();
        }       
    }
}
