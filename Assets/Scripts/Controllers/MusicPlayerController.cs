﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayerController : MonoBehaviour {

    // Music player created when game loaded, persist and plays through different scenes 

    static int musicPlayerCount = 0;

    void Awake () {

        if (musicPlayerCount != 0) {
            Destroy (gameObject);
        }
        else {
            musicPlayerCount++;
            DontDestroyOnLoad (gameObject);
        }
    }
}
