﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

    public void LoadGame () {
        SceneManager.LoadScene ("Main");
    }

    public void LoadInstructions () {
        SceneManager.LoadScene ("Instructions");
    }

    public void LoadMenu () {
        SceneManager.LoadScene ("Menu");
    }

    public void QuitQame () {
        Application.Quit ();
    }
}
