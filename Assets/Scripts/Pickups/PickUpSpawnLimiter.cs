﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpSpawnLimiter : MonoBehaviour {

    // This limiter makes sure that only one buff can spawn at a time to one location

    public bool spawnAvailable;
    
    void Start () {

        spawnAvailable = true;
    }
    
    public void setAvailabilityToFalse () {

        spawnAvailable = false;
    }

    public void setAvailabilityToTrue () {

        spawnAvailable = true;
    }
}
