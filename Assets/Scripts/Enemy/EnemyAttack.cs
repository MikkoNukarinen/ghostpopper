﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour {

    // All enemies use melee attacks, with a timer between them to simulate the time it takes to swing

    public float timeBetweenAttacks = 0.5f;
    public int attackDamage = 1;

    PlayerHealth playerHealth;
    EnemyHealth enemyHealth;
    GameObject player;
    bool playerInRange;
    float timer;

    void Awake () {

        player = GameObject.FindGameObjectWithTag ("Player");
        playerHealth = player.GetComponent<PlayerHealth> ();
        enemyHealth = GetComponent<EnemyHealth> ();
    }

    // Determine if the player is within melee range
    void OnCollisionEnter (Collision collision) {
                
        if (collision.gameObject.tag == ("Player")) {
            playerInRange = true;
        }    
    }

    void OnCollisionExit (Collision collision) {

        if (collision.gameObject.tag == ("Player")) {
            playerInRange = false;
        }
    }

    void Update () {

        timer += Time.deltaTime;

        // For the attack to occur, enough time must have passed between previous attack, and the player must be alive and within reach
        if (timer >= timeBetweenAttacks && enemyHealth.currentHealth > 0 && playerInRange) {
            Attack ();
        }    
    }

    void Attack () {
        timer = 0f;

        if (playerHealth.currentHealth > 0) {
            playerHealth.TakeDamage (attackDamage);
        }
    }
}
