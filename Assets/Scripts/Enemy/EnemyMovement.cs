﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

    // Controls the enemies' movement using the navigation mesh

    Transform player;
    PlayerHealth playerHealth;
    UnityEngine.AI.NavMeshAgent nav;

    void Awake () { 
        
        player = GameObject.FindGameObjectWithTag ("Player").transform;
        playerHealth = player.GetComponent<PlayerHealth> ();
        nav = GetComponent<UnityEngine.AI.NavMeshAgent> ();              
    }

    void Update () {

        if (playerHealth.currentHealth > 0) {
            nav.SetDestination (player.position);
        }        
    }
}
