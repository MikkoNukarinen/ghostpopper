﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {

    // Controls harming and killing the enemies

    public int startingHealth;
    public int currentHealth;

    bool isDead;
    AudioSource enemyAudio;
    GameController gameController;


    void Awake () {

        currentHealth = startingHealth;
        enemyAudio = GetComponent<AudioSource> ();
        gameController = GameObject.FindObjectOfType<GameController> ();
    }

    public void TakeDamage (int damage) {

        if (isDead) {
            return;
        }
        
        currentHealth -= damage;

        enemyAudio.Play ();

        if (currentHealth <= 0) {
            Die ();
        }

    }

    void Die () {

        isDead = true;

        // Each enemy killed increases player's score
        gameController.increaseScore ();

        Destroy (gameObject);    
    }
}
