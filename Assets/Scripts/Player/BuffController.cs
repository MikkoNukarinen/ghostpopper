﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffController : MonoBehaviour {

    // This script handles picking up the buffs and playing the sound effects, their execution is done in respective scripts 
    
    FireController fireController;
    PlayerMovement playerMovement;
    PlayerHealth playerHealth;

    AudioSource speedBoostAudio;
    AudioSource fireRateBoostAudio;
    AudioSource invincibilityBoostAudio;


    void Awake () {

        fireController = GameObject.FindObjectOfType<FireController> ();
        playerMovement = GameObject.FindObjectOfType<PlayerMovement> ();
        playerHealth = GameObject.FindObjectOfType<PlayerHealth> ();

        speedBoostAudio = GameObject.FindGameObjectWithTag ("SpeedBoostPlayer").GetComponent<AudioSource> ();
        fireRateBoostAudio = GameObject.FindGameObjectWithTag ("FireRateBoostPlayer").GetComponent<AudioSource> ();
        invincibilityBoostAudio = GameObject.FindGameObjectWithTag ("InvincibilityBoostPlayer").GetComponent<AudioSource> ();
    }

    void OnTriggerEnter (Collider other) {

        // When a player gets a pick up, that spawn location is set available for another pick up to be spawned
        if (other.tag == "PickUpSpawn") {
            other.gameObject.GetComponent<PickUpSpawnLimiter> ().setAvailabilityToTrue ();
        }

        if (other.tag == "FireRateBuff") {
            fireController.fireRateBuff ();
            other.gameObject.SetActive (false);
            fireRateBoostAudio.Play ();
        }
        
        else if (other.tag == "MovementSpeedBuff") {
            playerMovement.movementSpeedBuff();
            other.gameObject.SetActive (false);
            speedBoostAudio.Play ();
        }

        else if (other.tag == "InvincibilityBuff") {
            playerHealth.invincibilityBuff ();
            other.gameObject.SetActive (false);
            invincibilityBoostAudio.Play ();
        }
    }
}
