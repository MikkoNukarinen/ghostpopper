﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

    public float bulletSpeed;
    public int bulletDamage = 1;
    
    EnemyHealth enemyHealth;

    void Start () {

        Destroy (gameObject, 5f);    
    }

    void Update () {

        transform.Translate (Vector3.forward * bulletSpeed * Time.deltaTime);
    }

    void OnCollisionEnter (Collision collision) {
        
        if (collision.collider.tag == "Enemy") {
            enemyHealth = collision.gameObject.GetComponent<EnemyHealth> ();
            enemyHealth.TakeDamage (bulletDamage);
        }
        Destroy (gameObject, 0.02f);
    }    
}
