﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireController : MonoBehaviour {

    public GameObject bullet;    
    public Transform barrelEnd;
    public float delayBetweenShots;
    public float nextShot;
    public float bulletSpeed;

    AudioSource gunAudio;
    bool buffActive;
    float buffTimer;

    void Awake () {

        gunAudio = GetComponent<AudioSource> ();    
    }

    void Start () {

        buffTimer = 0f;
        nextShot = 0f;
    }

    void Update () {

        if (Input.GetButton ("Fire1")) {

            // Limit the player's fire rate

            if (Time.time > nextShot) {

                Instantiate (bullet, barrelEnd.position, barrelEnd.rotation);
                gunAudio.Play ();
                nextShot = Time.time + delayBetweenShots;
            }
        }

        if (buffActive) {
            buffTimer += Time.deltaTime;
        }

        if (buffTimer >= 10f) {              
            disableBuff ();
        }        
    }

    // Fire Rate Buff

    public void fireRateBuff () {

        buffTimer = 0f;
        buffActive = true;
        delayBetweenShots = delayBetweenShots / 2;
    }

    void disableBuff () {

        buffActive = false;
        buffTimer = 0f;
        delayBetweenShots = delayBetweenShots * 2;
    }
}
