﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {

    public int startingHealth = 10;
    public int currentHealth;
    public Slider healthSlider;
    public Image damageImage;
    public float flashSpeed = 5f;
    public Color flashColor = new Color (1f, 0f, 0f, 0.1f);
    public Text buffText;

    PlayerMovement playerMovement;
    AudioSource playerAudio;
    GameController gameController;
    bool damaged;
    bool isDead;
    bool buffActive;
    float buffTimer;

    void Awake () {

        currentHealth = startingHealth;
        playerMovement = GetComponent<PlayerMovement> ();
        playerAudio = GetComponent<AudioSource> ();
        gameController = GameObject.FindObjectOfType<GameController> ();
    }


    void Update () {

        // Flash the screen to indicate damage taken

        if (damaged) {
            damageImage.color = flashColor;
        }

        else {
            damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        damaged = false;

        // Manage how long invincibility buff lasts

        if (buffActive) {            
            buffTimer += Time.deltaTime;
        }

        if (buffTimer >= 10f) {
            disableBuff ();
        }
    }

    // Damaging and killing the player

    public void TakeDamage (int damage) {
       
        if (!buffActive) {
            damaged = true;

            currentHealth -= damage;
            playerAudio.Play ();

            healthSlider.value = currentHealth;

            if (currentHealth <= 0 && !isDead) {
                Die ();
            }
        }        
    }

    void Die () {

        isDead = true;

        playerMovement.enabled = false;
        Destroy (gameObject, 1f);

        gameController.showGameOver ();
    }

    // Invincibility buff

    public void invincibilityBuff () {

        buffTimer = 0f;
        buffActive = true;
        buffText.text = "INVINCIBLE";
    }

    void disableBuff () {

        buffActive = false;
        buffTimer = 0f;
        buffText.text = "";
    }
}
