﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float movementSpeed;

    Rigidbody playerRigidbody;
    Vector3 movement;
    float moveHorizontal;
    float moveVertical;
    int floorMask;
    float camRayLenght = 100f;
    Animator anim;
    bool buffActive;
    float buffTimer;

    void Awake () {

        playerRigidbody = GetComponent<Rigidbody> ();
        floorMask = LayerMask.GetMask ("Floor");
        anim = GetComponentInChildren<Animator> ();
    }
    void Update () {

        moveHorizontal = Input.GetAxisRaw ("Horizontal");
        moveVertical = Input.GetAxisRaw ("Vertical");
        
        if (buffActive) {
            buffTimer += Time.deltaTime;
        }

        if (buffTimer >= 10f) {
            disableBuff();
        }        
    }

    void FixedUpdate () {

        MovePlayer ();
        TurnPlayer ();
        Animation (moveHorizontal, moveVertical);
    }

    void MovePlayer () {

        movement.Set (moveHorizontal, 0f, moveVertical);
        movement = movement.normalized * movementSpeed * Time.deltaTime;
        playerRigidbody.MovePosition (transform.position + movement);
    }

    void TurnPlayer () {

        Ray camRay = Camera.main.ScreenPointToRay (Input.mousePosition);
        RaycastHit floorHit;

        if (Physics.Raycast (camRay, out floorHit, camRayLenght, floorMask)) {            
            Vector3 playerToMouse = floorHit.point - transform.position;
            playerToMouse.y = 0f;

            Quaternion newRotation = Quaternion.LookRotation (playerToMouse);
            playerRigidbody.MoveRotation (newRotation);
        }
    }

    void Animation (float horizontal, float vertical) {

        bool isWalking = horizontal != 0f || vertical != 0f;
        anim.SetBool ("IsWalking", isWalking);
    }

    public void movementSpeedBuff () {

        buffTimer = 0f;
        buffActive = true;
        movementSpeed = 7f;
    }

    void disableBuff () {

        buffActive = false;
        buffTimer = 0f;
        movementSpeed = 5f;
    }
}
